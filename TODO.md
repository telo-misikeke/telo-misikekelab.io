TODO
====

- [ ] Merge the CLI by jan Pawasi
- [ ] Find a way to somewhat handle uppercase text
    - akesi Ale: How about this: check if it's an all caps of an
      official word. If it is, don't complain. Otherwise, complain
      about the case of the name.
- [ ] Dismiss some specific errors for the duration of a session
- [ ] Show a `✔ ale li pona` notice under the text when there is no error
- [ ] Suggest corrections: levenstein + other obious things
- [ ] Structure: split into sentences beforehand to speed up the parsing process
- [ ] Directory of errors: encyclopedia with examples
- [ ] Buttons: Clear example text + Show new example
- [ ] Make a suggestion: explain the spirit of what is accepted and what is rejected
- [ ] The length of the textarea always stays at the maximum length used
- [ ] A common style is to add the original non-toki pona name in
      parenthesis right after the tokiponized name jan Alan Tuwin
      (Alan Turing). This could be ignored when matching
- [ ] mi ken lukin ala e ni
    - Nitpick: double check, "ken ijo ala" => "ken ala ijo"
    - Those are two different constructs, need to see if the funky one
      is over-used and is a mistake most of the time
    - Other preverbs?
- [ ] Nitpick: jan li wile e ni: tomo tawa
    - Might be a bit common for beginners, but not really for experienced speakers
    - nasin mi la, there are some use cases for "e ni:" without a full verb after
    => jan lawa lili li lon poka ni: mun 325 en mun 326 en mun 327 en mun 328
- [ ] Stress tests by jan Polijan
    - [x] Out of place particles
      - [x] pi mi pona
      - [x] sina wile pi.
      - [x] nanpa anu li?
    - [x] toki pi pona anu seme?
